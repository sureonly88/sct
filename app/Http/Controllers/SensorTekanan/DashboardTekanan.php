<?php

namespace App\Http\Controllers\SensorTekanan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Response;

class DashboardTekanan extends Controller
{
    public function index(){
        return view('sensor_tekanan.dashboard');
    }

    public function summaryTekanan(){
        $jmlSensor  = DB::table("GISPDAMBDM.GIS_WEB_SENSOR")->count();
        $avgTekanan = DB::table('GISPDAMBDM.GIS_WEB_SENSOR')->avg('TEKANAN');
        $maxTekanan = DB::table('GISPDAMBDM.GIS_WEB_SENSOR')->max('TEKANAN');
        $minTekanan = DB::table('GISPDAMBDM.GIS_WEB_SENSOR')->min('TEKANAN');

        $response['JmlSensor'] = $jmlSensor;
        $response['AvgTekanan'] = (substr(round($avgTekanan,2),0,1))=="."?'0'.round($avgTekanan,2):round($avgTekanan,2);
        $response['MaxTekanan'] = (substr($maxTekanan,0,1))=="."?'0'.$maxTekanan:$maxTekanan;
        $response['MinTekanan'] = (substr($minTekanan,0,1))=="."?'0'.$minTekanan:$minTekanan;

        return Response::json(array(
            'status' => true,
            'message' => '-',
            'data' => $response,
        ),200);
    }

    public function getSensor($kriteria){

        $dSensor = DB::table('GISPDAMBDM.GIS_WEB_SENSOR_WKT');

        switch($kriteria){
            case 2:
                $dSensor = $dSensor->whereBetween('TEKANAN', [0.50, 0.80])->where('MINUTES_ELAPSE',"<=", 10);
                break;
            case 3:
                $dSensor = $dSensor->where('TEKANAN',">", 0.8)->where('MINUTES_ELAPSE',"<=", 10);
                break;
            case 4:
                $dSensor = $dSensor->where('TEKANAN',"<", 0.5)->where('MINUTES_ELAPSE',"<=", 10);
                break;
            case 5:
                $dSensor = $dSensor->where('MINUTES_ELAPSE',">", 10);
                break;
        }
        $dSensor = $dSensor->get();

        return Response::json(array(
            'status' => true,
            'message' => '-',
            'data' => $dSensor,
        ),200);
    }

    public function getListSensor(){

        $dSensor = DB::table('GISPDAMBDM.GIS_WEB_SENSOR_WKT')
            ->select("ID","NAMA_SENSOR","KODE_SENSOR")
            ->get();

        //dd($dSensor);return;

        return Response::json(array(
            'status' => true,
            'message' => '-',
            'data' => $dSensor,
        ),200);
    }

    public function grafikTek($id,$tanggal,$jamawal,$jamakhir,$jenisData){

        $tglawal = $tanggal . ((strlen($jamawal) <= 7)?" 0":" ") . $jamawal;
        $tglakhir = $tanggal . ((strlen($jamakhir) <= 7)?" 0":" ") . $jamakhir;

        //echo $tglawal . " " . $tglakhir; die();

        $detailTekanan = DB::table("GISPDAMBDM.GIS_WEB_SENSOR_DETAIL")
            ->where("ID_SENSOR",$id)
            ->whereBetween('DATE_SERVER',[$tglawal, $tglakhir])->get();

        $data = array();
        $label = array();
        foreach ($detailTekanan as $detail) {
            //$data = array($detail->time_server, $detail->tekanan_in);
            // $vDate = explode(" ", $detail->date_server);
            // $vTgl = $vDate[0];
            // $vJam = $vDate[1];

            // $vsTgl = explode("-",$vTgl);
            // $vsJam = explode(":",$vJam);

            $dt['x'] = strtotime($detail->date_server) * 1000;

            switch($jenisData){
                case 1:
                    $dt['y'] = $detail->tekanan_in;
                    break;
                case 2:
                    $dt['y'] = $detail->tekanan_out;
                    break;
                case 3:
                    $dt['y'] = $detail->totalizer;
                    break;
                case 4:
                    $dt['y'] = $detail->flow;
                    break;
                default:
                    $dt['y'] = 0;
            }
            

            array_push($data, $dt);
            //array_push($label, str_replace(":","",$detail->time_server) );
        }

        return Response::json(array(
            'status' => true,
            'message' => '-',
            'data' => $data,
            'label' => $label,
        ),200);
    }


    public function grafikTek2($id,$tanggal,$jamawal,$jamakhir,$jenisData){

        $tglawal = $tanggal . ((strlen($jamawal) <= 7)?" 0":" ") . $jamawal;
        $tglakhir = $tanggal . ((strlen($jamakhir) <= 7)?" 0":" ") . $jamakhir;

        //echo $tglawal . " " . $tglakhir; die();
        $arrayId = explode(",", $id);

        $arrSensor = array();

        $data = array();

        $listSensor = DB::table("GISPDAMBDM.GIS_WEB_SENSOR")
            ->select("ID","NAMA_SENSOR")
            ->whereIn("ID",$arrayId)->get();

        foreach ($listSensor as $sensor) {

            $dtSensor['name'] = $sensor->nama_sensor;
            $dtSensor['data'] = array();

            $detailTekanan = DB::table("GISPDAMBDM.GIS_WEB_SENSOR_DETAIL")
                ->where("ID_SENSOR",$sensor->id)
                ->whereBetween('DATE_SERVER',[$tglawal, $tglakhir])->get();

            foreach ($detailTekanan as $detail) {

                $dt['x'] = strtotime($detail->date_server) * 1000;

                switch($jenisData){
                    case 1:
                        $dt['y'] = $detail->tekanan_in;
                        break;
                    case 2:
                        $dt['y'] = $detail->tekanan_out;
                        break;
                    case 3:
                        $dt['y'] = $detail->totalizer;
                        break;
                    case 4:
                        $dt['y'] = $detail->flow;
                        break;
                    default:
                        $dt['y'] = 0;
                }
                

                array_push($dtSensor['data'], $dt);
            }
            array_push($data, $dtSensor);

        }

        return Response::json(array(
            'status' => true,
            'message' => '-',
            'data' => $data
        ),200);
    }

    public function statusTek(){
        $TekananNormal  = DB::table("GISPDAMBDM.GIS_WEB_SENSOR")->whereBetween('TEKANAN', [0.50, 0.80])->where('MINUTES_ELAPSE',"<=", 10)->count();
        $TekananRendah = DB::table('GISPDAMBDM.GIS_WEB_SENSOR')->where('TEKANAN',"<", 0.5)->where('MINUTES_ELAPSE',"<=", 10)->count();
        $TekananTinggi = DB::table('GISPDAMBDM.GIS_WEB_SENSOR')->where('TEKANAN',">", 0.8)->where('MINUTES_ELAPSE',"<=", 10)->count();
        $SensorError = DB::table('GISPDAMBDM.GIS_WEB_SENSOR')->where('MINUTES_ELAPSE',">", 10)->count();

        $response['TekananNormal'] = $TekananNormal;
        $response['TekananRendah'] = $TekananRendah;
        $response['TekananTinggi'] = $TekananTinggi;
        $response['SensorError'] =  $SensorError;

        return Response::json(array(
            'status' => true,
            'message' => '-',
            'data' => $response,
        ),200);
    }
}
