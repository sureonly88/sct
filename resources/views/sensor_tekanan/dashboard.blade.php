<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sensor Tekanan | PDAM Bandarmasih</title>

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="{{ URL::asset('ol/ol.css') }}" type="text/css">

  <link rel="stylesheet" href="{{ URL::asset('lte/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('lte/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('lte/bower_components/Ionicons/css/ionicons.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('lte/dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('lte/dist/css/skins/_all-skins.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('/lte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('/lte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('/lte/plugins/timepicker/bootstrap-timepicker.min.css') }}">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <script src="{{ URL::asset('lte/bower_components/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ URL::asset('lte/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  <script src="{{ URL::asset('lte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
  <script src="{{ URL::asset('lte/bower_components/fastclick/lib/fastclick.js') }}"></script>
  <script src="{{ URL::asset('js/moment.js') }}"></script>
  <script src="{{ URL::asset('lte/dist/js/adminlte.min.js') }}"></script>

  <script src="{{ URL::asset('lte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <script src="{{ URL::asset('lte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ URL::asset('lte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>

  <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=requestAnimationFrame,Element.prototype.classList,URL"></script>
  <script src="{{ URL::asset('ol/ol.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('ol/proj4.js') }}"></script>

  <style>
    .example-modal .modal {
      position: relative;
      top: auto;
      bottom: auto;
      right: auto;
      left: auto;
      display: block;
      z-index: 1;
    }

    .example-modal .modal {
      background: transparent !important;
    }
  </style>
</head>

<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper" id="app">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
            <a href="" class="navbar-brand">SISTEM CONTROL TEKANAN</a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                <i class="fa fa-bars"></i>
            </button>
        </div>

        <menu-component></menu-component>
        <notification-component></notification-component>

      </div>

    </nav>
  </header>

  <div class="content-wrapper">


      <section class="content">

        <summary-component></summary-component>
        <map-component></map-component>
         
      </section>

  </div>

  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Version</b> Beta 1.0
      </div>
      <strong>Copyright &copy; 2019 <a href="">IT PDAM Bandarmasih</a>.</strong>
    </div>

  </footer>
</div>

<script src="{{ URL::asset('js/app.js') }}"></script>

</body>
</html>
