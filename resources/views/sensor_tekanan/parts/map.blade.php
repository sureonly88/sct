<div class="row">
    <!-- Left col -->
    <div class="col-md-9">
      <!-- MAP & BOX PANE -->
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Peta Persebaran Sensor Tekanan</h3>


          <div class="box-body no-padding">
            <div style="min-height:360px"></div>
          </div>

        </div>

      </div>
    </div>

    <div class="col-md-3">
            <!-- Info Boxes Style 2 -->
            <div class="info-box bg-aqua">
              <span class="info-box-icon"><i class="fa fa-check"></i></span>
  
              <div class="info-box-content">
                <span class="info-box-text">Tekanan Normal</span>
                <span class="info-box-number">10 Sensor</span>
  
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            <div class="info-box bg-green">
              <span class="info-box-icon"><i class="fa fa-angle-double-up"></i></span>
  
              <div class="info-box-content">
                <span class="info-box-text">Tekanan Tinggi</span>
                <span class="info-box-number">5 Sensor</span>
  
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            <div class="info-box bg-yellow">
              <span class="info-box-icon"><i class="fa fa-angle-double-down"></i></span>
  
              <div class="info-box-content">
                <span class="info-box-text">Tekanan Rendah</span>
                <span class="info-box-number">3 Sensor</span>
  
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            <div class="info-box bg-red">
              <span class="info-box-icon"><i class="fa fa-close"></i></span>
  
              <div class="info-box-content">
                <span class="info-box-text">Sensor Error</span>
                <span class="info-box-number">2 Sensor</span>
  

              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
    </div>
</div>
