<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::namespace('SensorTekanan')->prefix('tekanan')->group(function () {
    
    Route::get('/', 'DashboardTekanan@index');
    Route::get('/summary_tek', 'DashboardTekanan@summaryTekanan');
    Route::get('/status_tek', 'DashboardTekanan@statusTek');
    Route::get('/grafik_tek/{id}/{tanggal}/{jamawal}/{jamakhir}/{jenisdata}', 'DashboardTekanan@grafikTek');
    Route::get('/grafik_tek2/{id}/{tanggal}/{jamawal}/{jamakhir}/{jenisdata}', 'DashboardTekanan@grafikTek2');
    Route::get('/get_sensor/{kriteria}', 'DashboardTekanan@getSensor');
    Route::get('/get_list_sensor', 'DashboardTekanan@getListSensor');

});
